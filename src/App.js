import React, { useState, useEffect } from 'react';
import Web3 from 'web3'
import { connectWallet, detectWindowEthereum, getABIcontract, requestChainNetwork } from './helpers/web3';
import MTCRandomness from './truffle_abis/MTCRandomPrototype.json'
import EthereumTx from 'ethereumjs-tx'
import './App.css'


function App() {
  const networkId = 97
  const [MTCRandomnessContract, setMTCRandomnessContract] = useState(null)
  const [accountAddress, setAccountAddress] = useState('')
  const [teams, setTeams] = useState([]);
  const [winnerAddress, setWinnerName] = useState('')
  const [lastRequestID, setLastRequestID] = useState('')
  const [receivedAddress, setReceivedAddress] = useState('')

  useEffect(() => {
    const web3Network = detectWindowEthereum()
    if(web3Network) {
      getWalletAddress()
      readExistedContract(web3Network)
    } else {
      alert('download metamask first')
    }
  }, []);

  useEffect(() => {
    if(MTCRandomnessContract){
        LoadContractInfo()
    }
  },[MTCRandomnessContract])

  useEffect(() => {
    if(lastRequestID !== ''){
      const timer = setInterval(async() => await getLastWinner(timer), 20000)
    }
  }, [lastRequestID])

  async function getWalletAddress() {
    try {
      const wallet = await connectWallet()
      setAccountAddress(wallet)
    } catch (error) {
      alert(`Connect wallet failed :` + error)
    }
  }


  async function readExistedContract(web3Network) {
    try {
      const isChainAdded = await requestChainNetwork('BSC TESTNET')
      if (isChainAdded) {
        const mtcRandomnessContract = MTCRandomness.networks[networkId]
        if (mtcRandomnessContract) {
          const mtcrContract = new web3Network.eth.Contract(MTCRandomness.abi, MTCRandomness.networks[networkId].address)
          setMTCRandomnessContract(mtcrContract)
        } else {
          throw "Contract Not Found"
        }
      }
    }
    catch (err) {
      alert(err)
    }
  }


  async function LoadContractInfo() {
    try {
      if (MTCRandomnessContract) {
        const teams = await MTCRandomnessContract.methods.getTeamList().call()
        setTeams(teams)
        
        const lastID = await MTCRandomnessContract.methods.lastRequestId().call()
        setLastRequestID(lastID)

      } else {
        throw('contract not deployed')
      }
    } catch (error) {
      alert(error)
    }
  }

  async function getLastWinner(timer) {
    const lastWinnerDetail = await MTCRandomnessContract.methods.getWinnerDetails(lastRequestID).call()
    console.log(lastWinnerDetail, 'lastWinnerDetail');
    if(lastWinnerDetail.fulfilled){
      setWinnerName(lastWinnerDetail[1])
      clearInterval(timer)
      console.log('stopped');
    } else {
      console.log('false')
      setWinnerName('waiting Response From VRF')
    }
  }
  

  async function addteam(e){
    e.preventDefault()
    const teamName = e.target.teamname.value
    try {
      await MTCRandomnessContract.methods.addTeam(teamName).send({ from: accountAddress}).then(_ => {
        LoadContractInfo()
      })
    } catch (error) {
      alert(error)
    }
  }

  async function removeTeam(name){
    try {
      await MTCRandomnessContract.methods.removeTeam(name).send({ from: accountAddress}).then(_ => {
        LoadContractInfo()
      })
    } catch (error) {
      alert(error)
    }
  }

  async function randomWinner(){
    try {
      // await MTCRandomnessContract.methods.randomizeWinner().estimateGas({ from: accountAddress }).then(async response => {
      await MTCRandomnessContract.methods.randomizeWinner().send({ from: accountAddress }).then(async response => {
        const requestId = response.events.gacha.returnValues.requestId
        console.log(requestId, 'requestId');
        setLastRequestID(requestId)
      })
    } catch (error) {
      console.log(error, 'errrrr');
      alert(error)
    }
  }

  async function randomWinnerWithoutMetamask(){
    try {
      const BSCSCANAPIKEY = '29R6E2JZPDMXY62U7KBJ85X5EAP6JJJ6RB'
      const contractAddress = '0x66b2C5b90be05186DC5CEF8490042cc555D00CF5'
      const BSC = `https://data-seed-prebsc-1-s1.binance.org:8545`;
      const web3prov = new Web3(new Web3.providers.HttpProvider(`${BSC}/${BSCSCANAPIKEY}`))
      web3prov.eth.defaultAccount = '0x9C6a7c5Abd96404F830D0577ceeA79D18bf9ECD9';
      const PK = 'ef3a7cf213576d4b7f98f054bdafc7cfbf090e9398747ea2e9e581e0e96e542c'
      const contract = new web3prov.eth.Contract(MTCRandomness.abi, contractAddress, {
        from: web3prov.eth.defaultAccount ,
        gas: 3000000,
      })
      const functionAbi = await contract.methods.randomizeWinner().encodeABI()
      const transCount = await web3prov.eth.getTransactionCount(web3prov.eth.defaultAccount)
      
      var details = {
        "nonce": transCount,
        "gasPrice": web3prov.utils.toHex(web3prov.utils.toWei('47', 'gwei')),
        "gas": 300000,
        "to": '0x66b2C5b90be05186DC5CEF8490042cc555D00CF5',
        "value": 0,
        "data": functionAbi,
      };

      const transaction = new EthereumTx(details);
      transaction.sign(Buffer.from(PK, 'hex'))
      
      var rawData = '0x' + transaction.serialize().toString('hex');

      web3prov.eth.sendSignedTransaction(rawData)
        .on('transactionHash', function(hash){
          console.log(['transferToStaging Trx Hash:' + hash]);
        })
        .on('receipt', async function(receipt){
          console.log(['transferToStaging Receipt:', receipt]);
          if (receipt.status){
            const requestId = await contract.methods.lastRequestId().call()
            console.log(requestId, 'requestId');
            const getWinner = async () => {
              return await contract.methods.getWinnerDetails(requestId).call()
            }
            const intervals = setInterval(async() => {
              const winners = await getWinner()
              console.log(winners, 'lastWinnerDetail');
              if(winners.fulfilled){
                setWinnerName(winners[1])
                clearInterval(intervals)
                console.log('stopped');
              }
            }, 5000);
          }
        })
        .on('error', console.error);

    } catch (error) {
      console.log(error, 'errrrr');
      alert(error)
    }
  }

  async function requestGasFeeBSC() {
    try {
      // Variables definition
      if ( !receivedAddress ) throw 'incorrect wallet address'
      const privKey ='ef3a7cf213576d4b7f98f054bdafc7cfbf090e9398747ea2e9e581e0e96e542c';
      const addressFrom = '0x9C6a7c5Abd96404F830D0577ceeA79D18bf9ECD9';
      const BSCSCANAPIKEY = '29R6E2JZPDMXY62U7KBJ85X5EAP6JJJ6RB'
      const BSC = `https://data-seed-prebsc-1-s1.binance.org:8545`;
      const web3prov = new Web3(new Web3.providers.HttpProvider(`${BSC}/${BSCSCANAPIKEY}`))

      // Create transaction
      console.log(`Attempting to make transaction from ${addressFrom} to ${receivedAddress}`);

      const createTransaction = await web3prov.eth.accounts.signTransaction(
          {
            from: addressFrom,
            to: receivedAddress,
            value: web3prov.utils.toWei('0.05', 'ether'),
            gas: '300000',
          },
          privKey
      );

      // Deploy transaction
      const createReceipt = await web3prov.eth.sendSignedTransaction(
          createTransaction.rawTransaction
      );
      console.log(`Transaction successful with hash: ${createReceipt.transactionHash}`);

    } catch (error) {
      console.log(error);
      alert (error)
    }
  }

  return (
    <div className='App'>
      <button onClick={() => connectWallet()}>connect</button>
      <p>{accountAddress}</p>
      <br />
      <h1>Teams</h1>
      {
        teams.map((el, idx) => {
          return (
            <div key={idx} className='row' style={{width: '300px'}}>
              <div>{idx + 1 + '. ' + el}</div>
              <button onClick={() => removeTeam(el)}>delete</button>
            </div>
          )
        })
      }
      <br />
      <form onSubmit={(e) => addteam(e)}>
        <input type={'text'} id={'teamname'}/>
        <button type='submit'>Tambah Team</button>
      </form>
      <br/>
      <button onClick={() => randomWinnerWithoutMetamask()}>Random Winner Without Metamask</button>
      <button onClick={() => randomWinner()}>Random Winner</button>
      <h1>LAST WINNER</h1>
      <p>{winnerAddress}</p>
      <input
        type={'text'}
        onChange={(e) => setReceivedAddress(e.target.value)}
      ></input>
      <button onClick={() => requestGasFeeBSC()}>Request FEE</button>
    </div>
  );
}

export default App;