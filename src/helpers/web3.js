import Web3, { utils } from 'web3'
import ChainNetwork from '../network/chainNetwork.json'
import axios from 'axios'

export const detectWindowEthereum = () => {
    if(window.ethereum){
        const windowWeb3 = new Web3(window.ethereum)
        return windowWeb3
    } else {
        return false
    }
}


export const requestChainNetwork = async (chainName) => {
    return new Promise((resolve, reject) => {
        const chainNetworkData = ChainNetwork[chainName]
        window.ethereum.request({
            method: 'wallet_switchEthereumChain',
            params: [{ chainId: utils.toHex(chainNetworkData.chainId) }]
        }).then(() => {
            resolve(true)
        }).catch(async err => {
            // This error code indicates that the chain has not been added to MetaMask
            if (err.code === 4902) {
                await window.ethereum.request({
                    method: 'wallet_addEthereumChain',
                    params: [{
                        "chainName": chainNetworkData.chainName,
                        "chainId": utils.toHex(chainNetworkData.chainId),
                        "nativeCurrency": chainNetworkData.nativeCurrency,
                        "rpcUrls": chainNetworkData.rpcUrls
                    }]
                }).then(() => resolve(true))
            } else {
                reject(err)
            }
        })
    })
}


export const connectWallet = async () => {
    return new Promise((resolve, reject) => {
        window.ethereum.request({
            method: 'eth_requestAccounts',
        }).then(data => {
            resolve(data[0])
        }).catch (error => {
            reject(error)
        })
    })
};


//To get accounts with private key
export const getAccountsFromPK = async (privateKey) => {
    const web3 = new Web3(window.ethereum)
    let account = web3.eth.accounts.privateKeyToAccount('0x'+ privateKey);
    return account
}


export const getABIcontract = async (contractAddress, apikey) => {
    try {
        const { data } = await axios.get(`https://api-testnet.bscscan.com/api?module=contract&action=getabi&address=${contractAddress}&apikey=${apikey}`)
        return JSON.parse(data.result)
    } catch (error) {
        return error
    }
}