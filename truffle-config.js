
const HDWalletProvider = require('@truffle/hdwallet-provider')
const mnemonic = 'example desk yard flame round exercise rubber test figure plate hard lucky'


require('babel-register');
require('babel-polyfill');

const BscliveNetwork = 'https://data-seed-prebsc-1-s1.binance.org:8545';
const BscliveNetworkId = 97;

const MaticTestNetwork = 'https://rpc-mumbai.maticvigil.com/'
const MaticTestChainId = 80001

const BSCSCANAPIKEY = '29R6E2JZPDMXY62U7KBJ85X5EAP6JJJ6RB'

module.exports = {
  plugins: [
    'truffle-plugin-verify'
  ],
  api_keys: {
    bscscan: BSCSCANAPIKEY
  },
  networks: {
    development: {
      host: "localhost",
      port: 7545,
      network_id: "*" // Match any network id
    },
    bsctestnet: {
      networkCheckTimeout: 10000,
      provider: () => new HDWalletProvider(mnemonic, BscliveNetwork),
      network_id: BscliveNetworkId
    },
    matictest: {
      networkCheckTimeout: 30000,
      provider: () => new HDWalletProvider(mnemonic, MaticTestNetwork),
      network_id: MaticTestChainId
    }
  },
  contracts_directory: './src/contracts/',
  contracts_build_directory: './src/truffle_abis/',
  compilers: {
    solc: {
      version: '^0.8.7',
      optimizer: {
        enabled: true,
        runs: 200
      },
    }
  }
}

